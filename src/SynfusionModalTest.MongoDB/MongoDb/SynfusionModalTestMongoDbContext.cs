using SynfusionModalTest.Masters.Categories;
using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace SynfusionModalTest.MongoDB;

[ConnectionStringName("Default")]
public class SynfusionModalTestMongoDbContext : AbpMongoDbContext
{
    public IMongoCollection<Category> Categories => Collection<Category>();

    /* Add mongo collections here. Example:
     * public IMongoCollection<Question> Questions => Collection<Question>();
     */

    protected override void CreateModel(IMongoModelBuilder modelBuilder)
    {
        base.CreateModel(modelBuilder);

        //builder.Entity<YourEntity>(b =>
        //{
        //    //...
        //});

        modelBuilder.Entity<Category>(b => { b.CollectionName = SynfusionModalTestConsts.DbTablePrefix + "Categories"; });
    }
}