using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using SynfusionModalTest.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;
using MongoDB.Driver.Linq;
using MongoDB.Driver;

namespace SynfusionModalTest.Masters.Categories
{
    public class MongoCategoryRepository : MongoCategoryRepositoryBase, ICategoryRepository
    {
        public MongoCategoryRepository(IMongoDbContextProvider<SynfusionModalTestMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        //Write your custom code...
    }
}