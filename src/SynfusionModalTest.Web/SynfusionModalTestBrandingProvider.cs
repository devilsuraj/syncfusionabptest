﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace SynfusionModalTest.Web;

[Dependency(ReplaceServices = true)]
public class SynfusionModalTestBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "SynfusionModalTest";
}
