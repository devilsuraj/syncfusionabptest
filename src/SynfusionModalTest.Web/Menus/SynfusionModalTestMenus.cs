namespace SynfusionModalTest.Web.Menus;

public class SynfusionModalTestMenus
{
    private const string Prefix = "SynfusionModalTest";

    public const string Home = Prefix + ".Home";

    public const string HostDashboard = Prefix + ".HostDashboard";

    public const string TenantDashboard = Prefix + ".TenantDashboard";

    public const string Categories = Prefix + ".Categories";

}