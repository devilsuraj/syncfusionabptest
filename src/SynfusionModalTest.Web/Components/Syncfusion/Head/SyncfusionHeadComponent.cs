﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace MyDhobi.Web.Components.Syncfusion.Head
{
    public class SyncfusionHeadComponent : AbpViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Components/Syncfusion/Head/Default.cshtml");
        }
    }
}