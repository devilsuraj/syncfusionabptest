using SynfusionModalTest.Web.Pages.Categories;
using Volo.Abp.AutoMapper;
using SynfusionModalTest.Masters.Categories;
using AutoMapper;

namespace SynfusionModalTest.Web;

public class SynfusionModalTestWebAutoMapperProfile : Profile
{
    public SynfusionModalTestWebAutoMapperProfile()
    {
        //Define your object mappings here, for the Web project

        CreateMap<CategoryDto, CategoryUpdateViewModel>();
        CreateMap<CategoryUpdateViewModel, CategoryUpdateDto>();
        CreateMap<CategoryCreateViewModel, CategoryCreateDto>();
    }
}