﻿using SynfusionModalTest.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace SynfusionModalTest.Web.Pages;

public abstract class SynfusionModalTestPageModel : AbpPageModel
{
    protected SynfusionModalTestPageModel()
    {
        LocalizationResourceType = typeof(SynfusionModalTestResource);
    }
}
