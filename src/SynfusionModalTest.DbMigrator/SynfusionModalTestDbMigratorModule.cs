﻿using SynfusionModalTest.MongoDB;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace SynfusionModalTest.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(SynfusionModalTestMongoDbModule),
    typeof(SynfusionModalTestApplicationContractsModule)
)]
public class SynfusionModalTestDbMigratorModule : AbpModule
{
}
