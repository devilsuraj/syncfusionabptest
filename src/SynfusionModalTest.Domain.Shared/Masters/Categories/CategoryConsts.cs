namespace SynfusionModalTest.Masters.Categories
{
    public static class CategoryConsts
    {
        private const string DefaultSorting = "{0}Title asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "Category." : string.Empty);
        }

    }
}