﻿using Volo.Abp.Localization;

namespace SynfusionModalTest.Localization;

[LocalizationResourceName("SynfusionModalTest")]
public class SynfusionModalTestResource
{

}
