﻿using SynfusionModalTest.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace SynfusionModalTest.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class SynfusionModalTestController : AbpControllerBase
{
    protected SynfusionModalTestController()
    {
        LocalizationResource = typeof(SynfusionModalTestResource);
    }
}
