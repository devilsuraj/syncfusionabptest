using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace SynfusionModalTest.Masters.Categories
{
    public abstract class CategoryUpdateDtoBase : IHasConcurrencyStamp
    {
        public string? Title { get; set; }
        public string? Description { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}