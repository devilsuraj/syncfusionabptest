using Volo.Abp.Application.Dtos;
using System;

namespace SynfusionModalTest.Masters.Categories
{
    public abstract class CategoryExcelDownloadDtoBase
    {
        public string DownloadToken { get; set; } = null!;

        public string? FilterText { get; set; }

        public string? Title { get; set; }
        public string? Description { get; set; }

        public CategoryExcelDownloadDtoBase()
        {

        }
    }
}