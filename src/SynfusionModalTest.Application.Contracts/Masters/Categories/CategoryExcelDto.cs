using System;

namespace SynfusionModalTest.Masters.Categories
{
    public abstract class CategoryExcelDtoBase
    {
        public string? Title { get; set; }
        public string? Description { get; set; }
    }
}