using Volo.Abp.Application.Dtos;
using System;

namespace SynfusionModalTest.Masters.Categories
{
    public abstract class GetCategoriesInputBase : PagedAndSortedResultRequestDto
    {

        public string? FilterText { get; set; }

        public string? Title { get; set; }
        public string? Description { get; set; }

        public GetCategoriesInputBase()
        {

        }
    }
}