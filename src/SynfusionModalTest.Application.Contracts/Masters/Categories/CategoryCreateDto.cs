using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace SynfusionModalTest.Masters.Categories
{
    public abstract class CategoryCreateDtoBase
    {
        public string? Title { get; set; }
        public string? Description { get; set; }
    }
}