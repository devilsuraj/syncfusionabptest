using SynfusionModalTest.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.MultiTenancy;

namespace SynfusionModalTest.Permissions;

public class SynfusionModalTestPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(SynfusionModalTestPermissions.GroupName);

        myGroup.AddPermission(SynfusionModalTestPermissions.Dashboard.Host, L("Permission:Dashboard"), MultiTenancySides.Host);
        myGroup.AddPermission(SynfusionModalTestPermissions.Dashboard.Tenant, L("Permission:Dashboard"), MultiTenancySides.Tenant);

        //Define your own permissions here. Example:
        //myGroup.AddPermission(SynfusionModalTestPermissions.MyPermission1, L("Permission:MyPermission1"));

        var categoryPermission = myGroup.AddPermission(SynfusionModalTestPermissions.Categories.Default, L("Permission:Categories"));
        categoryPermission.AddChild(SynfusionModalTestPermissions.Categories.Create, L("Permission:Create"));
        categoryPermission.AddChild(SynfusionModalTestPermissions.Categories.Edit, L("Permission:Edit"));
        categoryPermission.AddChild(SynfusionModalTestPermissions.Categories.Delete, L("Permission:Delete"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<SynfusionModalTestResource>(name);
    }
}