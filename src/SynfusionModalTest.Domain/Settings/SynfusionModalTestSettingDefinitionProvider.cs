﻿using Volo.Abp.Settings;

namespace SynfusionModalTest.Settings;

public class SynfusionModalTestSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(SynfusionModalTestSettings.MySetting1));
    }
}
