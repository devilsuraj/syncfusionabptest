using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace SynfusionModalTest.Masters.Categories
{
    public partial interface ICategoryRepository : IRepository<Category, Guid>
    {

        Task DeleteAllAsync(
            string? filterText = null,
            string? title = null,
            string? description = null,
            CancellationToken cancellationToken = default);
        Task<List<Category>> GetListAsync(
                    string? filterText = null,
                    string? title = null,
                    string? description = null,
                    string? sorting = null,
                    int maxResultCount = int.MaxValue,
                    int skipCount = 0,
                    CancellationToken cancellationToken = default
                );

        Task<long> GetCountAsync(
            string? filterText = null,
            string? title = null,
            string? description = null,
            CancellationToken cancellationToken = default);
    }
}