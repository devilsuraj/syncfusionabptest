using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace SynfusionModalTest.Masters.Categories
{
    public abstract class CategoryManagerBase : DomainService
    {
        protected ICategoryRepository _categoryRepository;

        public CategoryManagerBase(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public virtual async Task<Category> CreateAsync(
        string? title = null, string? description = null)
        {

            var category = new Category(
             GuidGenerator.Create(),
             title, description
             );

            return await _categoryRepository.InsertAsync(category);
        }

        public virtual async Task<Category> UpdateAsync(
            Guid id,
            string? title = null, string? description = null, [CanBeNull] string? concurrencyStamp = null
        )
        {

            var category = await _categoryRepository.GetAsync(id);

            category.Title = title;
            category.Description = description;

            category.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _categoryRepository.UpdateAsync(category);
        }

    }
}