using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;

namespace SynfusionModalTest.Masters.Categories
{
    public abstract class CategoryBase : FullAuditedAggregateRoot<Guid>
    {
        [CanBeNull]
        public virtual string? Title { get; set; }

        [CanBeNull]
        public virtual string? Description { get; set; }

        protected CategoryBase()
        {

        }

        public CategoryBase(Guid id, string? title = null, string? description = null)
        {

            Id = id;
            Title = title;
            Description = description;
        }

    }
}