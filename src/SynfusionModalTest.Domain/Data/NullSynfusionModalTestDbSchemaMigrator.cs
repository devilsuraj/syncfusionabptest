﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace SynfusionModalTest.Data;

/* This is used if database provider does't define
 * ISynfusionModalTestDbSchemaMigrator implementation.
 */
public class NullSynfusionModalTestDbSchemaMigrator : ISynfusionModalTestDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
