﻿using System.Threading.Tasks;

namespace SynfusionModalTest.Data;

public interface ISynfusionModalTestDbSchemaMigrator
{
    Task MigrateAsync();
}
