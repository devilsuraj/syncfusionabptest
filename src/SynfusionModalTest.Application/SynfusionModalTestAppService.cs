﻿using SynfusionModalTest.Localization;
using Volo.Abp.Application.Services;

namespace SynfusionModalTest;

/* Inherit your application services from this class.
 */
public abstract class SynfusionModalTestAppService : ApplicationService
{
    protected SynfusionModalTestAppService()
    {
        LocalizationResource = typeof(SynfusionModalTestResource);
    }
}
