using System;
using SynfusionModalTest.Shared;
using Volo.Abp.AutoMapper;
using SynfusionModalTest.Masters.Categories;
using AutoMapper;

namespace SynfusionModalTest;

public class SynfusionModalTestApplicationAutoMapperProfile : Profile
{
    public SynfusionModalTestApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */

        CreateMap<Category, CategoryDto>();
        CreateMap<Category, CategoryExcelDto>();
    }
}