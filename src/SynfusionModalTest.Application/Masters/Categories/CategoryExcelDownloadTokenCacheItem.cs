using System;

namespace SynfusionModalTest.Masters.Categories;

public abstract class CategoryExcelDownloadTokenCacheItemBase
{
    public string Token { get; set; } = null!;
}