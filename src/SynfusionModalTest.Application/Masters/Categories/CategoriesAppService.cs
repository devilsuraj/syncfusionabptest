using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using SynfusionModalTest.Permissions;
using SynfusionModalTest.Masters.Categories;
using MiniExcelLibs;
using Volo.Abp.Content;
using Volo.Abp.Authorization;
using Volo.Abp.Caching;
using Microsoft.Extensions.Caching.Distributed;
using SynfusionModalTest.Shared;

namespace SynfusionModalTest.Masters.Categories
{

    [Authorize(SynfusionModalTestPermissions.Categories.Default)]
    public abstract class CategoriesAppServiceBase : SynfusionModalTestAppService
    {
        protected IDistributedCache<CategoryExcelDownloadTokenCacheItem, string> _excelDownloadTokenCache;
        protected ICategoryRepository _categoryRepository;
        protected CategoryManager _categoryManager;

        public CategoriesAppServiceBase(ICategoryRepository categoryRepository, CategoryManager categoryManager, IDistributedCache<CategoryExcelDownloadTokenCacheItem, string> excelDownloadTokenCache)
        {
            _excelDownloadTokenCache = excelDownloadTokenCache;
            _categoryRepository = categoryRepository;
            _categoryManager = categoryManager;
        }

        public virtual async Task<PagedResultDto<CategoryDto>> GetListAsync(GetCategoriesInput input)
        {
            var totalCount = await _categoryRepository.GetCountAsync(input.FilterText, input.Title, input.Description);
            var items = await _categoryRepository.GetListAsync(input.FilterText, input.Title, input.Description, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<CategoryDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Category>, List<CategoryDto>>(items)
            };
        }

        public virtual async Task<CategoryDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<Category, CategoryDto>(await _categoryRepository.GetAsync(id));
        }

        [Authorize(SynfusionModalTestPermissions.Categories.Delete)]
        public virtual async Task DeleteAsync(Guid id)
        {
            await _categoryRepository.DeleteAsync(id);
        }

        [Authorize(SynfusionModalTestPermissions.Categories.Create)]
        public virtual async Task<CategoryDto> CreateAsync(CategoryCreateDto input)
        {

            var category = await _categoryManager.CreateAsync(
            input.Title, input.Description
            );

            return ObjectMapper.Map<Category, CategoryDto>(category);
        }

        [Authorize(SynfusionModalTestPermissions.Categories.Edit)]
        public virtual async Task<CategoryDto> UpdateAsync(Guid id, CategoryUpdateDto input)
        {

            var category = await _categoryManager.UpdateAsync(
            id,
            input.Title, input.Description, input.ConcurrencyStamp
            );

            return ObjectMapper.Map<Category, CategoryDto>(category);
        }

        [AllowAnonymous]
        public virtual async Task<IRemoteStreamContent> GetListAsExcelFileAsync(CategoryExcelDownloadDto input)
        {
            var downloadToken = await _excelDownloadTokenCache.GetAsync(input.DownloadToken);
            if (downloadToken == null || input.DownloadToken != downloadToken.Token)
            {
                throw new AbpAuthorizationException("Invalid download token: " + input.DownloadToken);
            }

            var items = await _categoryRepository.GetListAsync(input.FilterText, input.Title, input.Description);

            var memoryStream = new MemoryStream();
            await memoryStream.SaveAsAsync(ObjectMapper.Map<List<Category>, List<CategoryExcelDto>>(items));
            memoryStream.Seek(0, SeekOrigin.Begin);

            return new RemoteStreamContent(memoryStream, "Categories.xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        public virtual async Task<SynfusionModalTest.Shared.DownloadTokenResultDto> GetDownloadTokenAsync()
        {
            var token = Guid.NewGuid().ToString("N");

            await _excelDownloadTokenCache.SetAsync(
                token,
                new CategoryExcelDownloadTokenCacheItem { Token = token },
                new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30)
                });

            return new SynfusionModalTest.Shared.DownloadTokenResultDto
            {
                Token = token
            };
        }
        [Authorize(SynfusionModalTestPermissions.Categories.Delete)]
        public virtual async Task DeleteByIdsAsync(List<Guid> categoryIds)
        {
            await _categoryRepository.DeleteManyAsync(categoryIds);
        }

        [Authorize(SynfusionModalTestPermissions.Categories.Delete)]
        public virtual async Task DeleteAllAsync(GetCategoriesInput input)
        {
            await _categoryRepository.DeleteAllAsync(input.FilterText, input.Title, input.Description);
        }
    }
}