﻿mkcert --cert-file synfusionmodaltest-local.pem --key-file synfusionmodaltest-local-key.pem "synfusionmodaltest-local"  "synfusionmodaltest-local-web"   
kubectl create namespace synfusionmodaltest-local
kubectl create secret tls -n synfusionmodaltest-local synfusionmodaltest-local-tls --cert=./synfusionmodaltest-local.pem --key=./synfusionmodaltest-local-key.pem