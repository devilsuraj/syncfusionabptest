param (
	$Namespace="synfusionmodaltest-local",
    $ReleaseName="synfusionmodaltest-local"
)

helm uninstall ${ReleaseName} --namespace ${Namespace}