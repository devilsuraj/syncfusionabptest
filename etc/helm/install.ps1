param (
	$ChartName="synfusionmodaltest",
	$Namespace="synfusionmodaltest-local",
	$ReleaseName="synfusionmodaltest-local",
	$DotnetEnvironment="Staging"
)

# Create values.localdev.yaml if not exists
$localDevFilePath = Join-Path $PSScriptRoot "synfusionmodaltest/values.localdev.yaml"
if (!(Test-Path $localDevFilePath)) {
	New-Item -ItemType File -Path $localDevFilePath | Out-Null
}

# Install (or upgrade) the Helm chart
helm upgrade --install ${ReleaseName} ${ChartName} --namespace ${Namespace} --create-namespace --set global.dotnetEnvironment=${DotnetEnvironment} -f "synfusionmodaltest/values.localdev.yaml" -f "$ChartName/values.${ReleaseName}.yaml"