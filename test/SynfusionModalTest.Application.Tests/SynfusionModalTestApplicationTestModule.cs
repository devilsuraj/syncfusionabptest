﻿using Volo.Abp.Modularity;

namespace SynfusionModalTest;

[DependsOn(
    typeof(SynfusionModalTestApplicationModule),
    typeof(SynfusionModalTestDomainTestModule)
)]
public class SynfusionModalTestApplicationTestModule : AbpModule
{

}
