﻿using Volo.Abp.Modularity;

namespace SynfusionModalTest;

public abstract class SynfusionModalTestApplicationTestBase<TStartupModule> : SynfusionModalTestTestBase<TStartupModule>
    where TStartupModule : IAbpModule
{

}
