using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Modularity;
using Xunit;

namespace SynfusionModalTest.Masters.Categories
{
    public abstract class CategoriesAppServiceTests<TStartupModule> : SynfusionModalTestApplicationTestBase<TStartupModule>
        where TStartupModule : IAbpModule
    {
        private readonly ICategoriesAppService _categoriesAppService;
        private readonly IRepository<Category, Guid> _categoryRepository;

        public CategoriesAppServiceTests()
        {
            _categoriesAppService = GetRequiredService<ICategoriesAppService>();
            _categoryRepository = GetRequiredService<IRepository<Category, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _categoriesAppService.GetListAsync(new GetCategoriesInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.Id == Guid.Parse("5c46219f-40d1-4374-8786-af6b13f1adbc")).ShouldBe(true);
            result.Items.Any(x => x.Id == Guid.Parse("561b0d2b-b86b-40cd-a252-1214afc8df6a")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _categoriesAppService.GetAsync(Guid.Parse("5c46219f-40d1-4374-8786-af6b13f1adbc"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("5c46219f-40d1-4374-8786-af6b13f1adbc"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new CategoryCreateDto
            {
                Title = "b9aa37df5ec14d2084a6930acfc391b7dcbe82e138b94e1cb60876ae06ebc587c1390c6bf29",
                Description = "18b4b8a12334404e836a3bf"
            };

            // Act
            var serviceResult = await _categoriesAppService.CreateAsync(input);

            // Assert
            var result = await _categoryRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.Title.ShouldBe("b9aa37df5ec14d2084a6930acfc391b7dcbe82e138b94e1cb60876ae06ebc587c1390c6bf29");
            result.Description.ShouldBe("18b4b8a12334404e836a3bf");
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new CategoryUpdateDto()
            {
                Title = "7bd9a178e637",
                Description = "9545c1601ac84630830a3738ef35b475ea02a7a39c4945d2b124bf18060c2902510dde273"
            };

            // Act
            var serviceResult = await _categoriesAppService.UpdateAsync(Guid.Parse("5c46219f-40d1-4374-8786-af6b13f1adbc"), input);

            // Assert
            var result = await _categoryRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.Title.ShouldBe("7bd9a178e637");
            result.Description.ShouldBe("9545c1601ac84630830a3738ef35b475ea02a7a39c4945d2b124bf18060c2902510dde273");
        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _categoriesAppService.DeleteAsync(Guid.Parse("5c46219f-40d1-4374-8786-af6b13f1adbc"));

            // Assert
            var result = await _categoryRepository.FindAsync(c => c.Id == Guid.Parse("5c46219f-40d1-4374-8786-af6b13f1adbc"));

            result.ShouldBeNull();
        }
    }
}