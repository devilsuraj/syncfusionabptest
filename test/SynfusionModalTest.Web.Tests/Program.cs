using Microsoft.AspNetCore.Builder;
using SynfusionModalTest;
using Volo.Abp.AspNetCore.TestBase;

var builder = WebApplication.CreateBuilder();
await builder.RunAbpModuleAsync<SynfusionModalTestWebTestModule>();

public partial class Program
{
}
