﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace SynfusionModalTest.Pages;

[Collection(SynfusionModalTestTestConsts.CollectionDefinitionName)]
public class Index_Tests : SynfusionModalTestWebTestBase
{
    [Fact]
    public async Task Welcome_Page()
    {
        var response = await GetResponseAsStringAsync("/");
        response.ShouldNotBeNull();
    }
}
