using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using SynfusionModalTest.Masters.Categories;

namespace SynfusionModalTest.Masters.Categories
{
    public class CategoriesDataSeedContributor : IDataSeedContributor, ISingletonDependency
    {
        private bool IsSeeded = false;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public CategoriesDataSeedContributor(ICategoryRepository categoryRepository, IUnitOfWorkManager unitOfWorkManager)
        {
            _categoryRepository = categoryRepository;
            _unitOfWorkManager = unitOfWorkManager;

        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (IsSeeded)
            {
                return;
            }

            await _categoryRepository.InsertAsync(new Category
            (
                id: Guid.Parse("5c46219f-40d1-4374-8786-af6b13f1adbc"),
                title: "41e63a7168d145b4bfcbf7df53842da68958ef3e84dc48068",
                description: "c8577b61de1f4fd9"
            ));

            await _categoryRepository.InsertAsync(new Category
            (
                id: Guid.Parse("561b0d2b-b86b-40cd-a252-1214afc8df6a"),
                title: "31b27de629",
                description: "d97d11464090479983e47642b9a12cb4dd27277dc15745448f19"
            ));

            await _unitOfWorkManager!.Current!.SaveChangesAsync();

            IsSeeded = true;
        }
    }
}