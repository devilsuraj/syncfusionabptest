﻿using Volo.Abp.Modularity;

namespace SynfusionModalTest;

[DependsOn(
    typeof(SynfusionModalTestDomainModule),
    typeof(SynfusionModalTestTestBaseModule)
)]
public class SynfusionModalTestDomainTestModule : AbpModule
{

}
