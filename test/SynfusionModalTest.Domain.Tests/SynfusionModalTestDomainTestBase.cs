﻿using Volo.Abp.Modularity;

namespace SynfusionModalTest;

/* Inherit from this class for your domain layer tests. */
public abstract class SynfusionModalTestDomainTestBase<TStartupModule> : SynfusionModalTestTestBase<TStartupModule>
    where TStartupModule : IAbpModule
{

}
