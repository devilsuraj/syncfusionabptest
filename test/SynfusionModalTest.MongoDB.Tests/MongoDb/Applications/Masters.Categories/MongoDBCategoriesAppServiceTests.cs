using SynfusionModalTest.Masters.Categories;
using Xunit;

namespace SynfusionModalTest.MongoDB.Applications.Masters.Categories;

[Collection(SynfusionModalTestTestConsts.CollectionDefinitionName)]
public class MongoDBCategoriesAppServiceTests : CategoriesAppServiceTests<SynfusionModalTestMongoDbTestModule>
{
}