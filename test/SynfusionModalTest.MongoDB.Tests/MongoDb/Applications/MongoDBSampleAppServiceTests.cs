using SynfusionModalTest.MongoDB;
using SynfusionModalTest.Samples;
using Xunit;

namespace SynfusionModalTest.MongoDb.Applications;

[Collection(SynfusionModalTestTestConsts.CollectionDefinitionName)]
public class MongoDBSampleAppServiceTests : SampleAppServiceTests<SynfusionModalTestMongoDbTestModule>
{

}
