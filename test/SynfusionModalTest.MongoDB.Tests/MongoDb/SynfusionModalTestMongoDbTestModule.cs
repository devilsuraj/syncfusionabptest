using System;
using Volo.Abp.Data;
using Volo.Abp.Modularity;
using Volo.Abp.Uow;

namespace SynfusionModalTest.MongoDB;

[DependsOn(
    typeof(SynfusionModalTestApplicationTestModule),
    typeof(SynfusionModalTestMongoDbModule)
)]
public class SynfusionModalTestMongoDbTestModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpDbConnectionOptions>(options =>
        {
            options.ConnectionStrings.Default = SynfusionModalTestMongoDbFixture.GetRandomConnectionString();
        });
    }
}
