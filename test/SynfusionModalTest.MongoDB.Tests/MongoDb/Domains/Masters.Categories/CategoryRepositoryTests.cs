using Shouldly;
using System;
using System.Linq;
using System.Threading.Tasks;
using SynfusionModalTest.Masters.Categories;
using SynfusionModalTest.MongoDB;
using Xunit;

namespace SynfusionModalTest.MongoDB.Domains.Masters.Categories
{
    [Collection(SynfusionModalTestTestConsts.CollectionDefinitionName)]
    public class CategoryRepositoryTests : SynfusionModalTestMongoDbTestBase
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryRepositoryTests()
        {
            _categoryRepository = GetRequiredService<ICategoryRepository>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Arrange
            await WithUnitOfWorkAsync(async () =>
            {
                // Act
                var result = await _categoryRepository.GetListAsync(
                    title: "41e63a7168d145b4bfcbf7df53842da68958ef3e84dc48068",
                    description: "c8577b61de1f4fd9"
                );

                // Assert
                result.Count.ShouldBe(1);
                result.FirstOrDefault().ShouldNotBe(null);
                result.First().Id.ShouldBe(Guid.Parse("5c46219f-40d1-4374-8786-af6b13f1adbc"));
            });
        }

        [Fact]
        public async Task GetCountAsync()
        {
            // Arrange
            await WithUnitOfWorkAsync(async () =>
            {
                // Act
                var result = await _categoryRepository.GetCountAsync(
                    title: "31b27de629",
                    description: "d97d11464090479983e47642b9a12cb4dd27277dc15745448f19"
                );

                // Assert
                result.ShouldBe(1);
            });
        }
    }
}