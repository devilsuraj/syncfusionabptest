using SynfusionModalTest.Samples;
using Xunit;

namespace SynfusionModalTest.MongoDB.Domains;

[Collection(SynfusionModalTestTestConsts.CollectionDefinitionName)]
public class MongoDBSampleDomainTests : SampleDomainTests<SynfusionModalTestMongoDbTestModule>
{

}
