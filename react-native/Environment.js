const yourIP = 'Your Local IP Address etc 192.168.1.64'; // See the docs https://docs.abp.io/en/abp/latest/Getting-Started-React-Native?Tiered=No
const apiUrl = `http://${yourIP}:44324`;

const ENV = {
  dev: {
    apiUrl,
    appUrl: `exp://${yourIP}:19000`,
    oAuthConfig: {
      issuer: apiUrl,
      clientId: 'SynfusionModalTest_Mobile',
      scope: 'offline_access SynfusionModalTest',
    },
    localization: {
      defaultResourceName: 'SynfusionModalTest',
    },
  },
  prod: {
    apiUrl,
    appUrl: `exp://${yourIP}:19000`,
    oAuthConfig: {
      issuer: `http://${yourIP}:44324`,
      clientId: 'SynfusionModalTest_Mobile',
      scope: 'offline_access SynfusionModalTest',
    },
    localization: {
      defaultResourceName: 'SynfusionModalTest',
    },
  },
};

export const getEnvVars = () => {
  // eslint-disable-next-line no-undef
  return __DEV__ ? ENV.dev : ENV.prod;
};
